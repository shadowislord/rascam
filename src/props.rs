macro_rules! property_enum {
    ($prop_name:ident, $setter_name:ident, $prop_type:ty, $mmal_name:ident, $mmal_type:ty) => {
        /// Retrieve the property
        pub fn $prop_name(&self) -> Result<$prop_type> {
            let param : $mmal_type = self.control_port().get()?;
            Ok(unsafe { std::mem::transmute(param.$mmal_name) })
        }

        /// Set the property
        pub fn $setter_name(&mut self, value: $prop_type) -> Result<()> {
            let mut param = <$mmal_type>::new();
            param.$mmal_name = value as _;
            self.control_port().set(&mut param)?;
            Ok(())
        }
    };
}

macro_rules! property_non_enum {
    ($prop_name:ident, $setter_name:ident, $prop_type:ty, $mmal_param:expr, $mmal_setter:ident, $mmal_getter:ident) => {
        /// Retrieve the property
        pub fn $prop_name(&self) -> Result<$prop_type> {
            self.control_port()
                .$mmal_getter($mmal_param)
                .map_err(Into::into)
        }

        /// Set the property
        pub fn $setter_name(&mut self, value: $prop_type) -> Result<()> {
            self.control_port()
                .$mmal_setter($mmal_param, value)?;
            Ok(())
        }
    };
}

macro_rules! property_uint32 {
    ($prop_name:ident, $setter_name:ident, $mmal_param:expr) => {
        property_non_enum!(
            $prop_name,
            $setter_name,
            u32,
            $mmal_param,
            set_uint32,
            get_uint32
        );
    };
}

macro_rules! property_bool {
    ($prop_name:ident, $setter_name:ident, $mmal_param:expr) => {
        property_non_enum!(
            $prop_name,
            $setter_name,
            bool,
            $mmal_param,
            set_bool,
            get_bool
        );
    };
}