use crate::enums::IntraRefreshMode;
use crate::obj::{self, *};

type Result<T> = ::std::result::Result<T, crate::error::CameraError>;

/// Recording configuration.
pub struct Recording {
    bitrate: u32,
    low_latency: bool,
    intra_period: u32,
    intra_refresh_mode: Option<IntraRefreshMode>,
}

impl Default for Recording {
    fn default() -> Recording {
        Recording {
            bitrate: 1024 * 1024,
            low_latency: false,
            intra_period: 60,
            intra_refresh_mode: None,
        }
    }
}

impl Recording {
    pub fn builder() -> RecordingBuilder {
        RecordingBuilder {
            recording: Default::default(),
        }
    }
}

/// Builder for Recording.
pub struct RecordingBuilder {
    recording: Recording,
}

impl RecordingBuilder {
    pub fn bitrate(mut self, bitrate: u32) -> Self {
        self.recording.bitrate = bitrate;
        self
    }

    pub fn low_latency(mut self, low_latency: bool) -> Self {
        self.recording.low_latency = low_latency;
        self
    }

    pub fn intra_period(mut self, intra_period: u32) -> Self {
        self.recording.intra_period = intra_period;
        self
    }

    pub fn intra_refresh_mode(mut self, intra_refresh_mode: IntraRefreshMode) -> Self {
        self.recording.intra_refresh_mode = Some(intra_refresh_mode);
        self
    }

    pub fn build(self) -> Recording {
        self.recording
    }
}

pub(crate) struct Encoder {
    enc: Component,
}

impl Encoder {
    pub fn new(_width: u32, _height: u32, recording: Recording) -> Result<Self> {
        let mut enc = obj::Component::new(ffi::MMAL_COMPONENT_DEFAULT_VIDEO_ENCODER)?;
        Self::configure_encoder(&mut enc, recording.bitrate)?;
        Self::set_intra_period(&mut enc, recording.intra_period)?;
        Self::set_low_latency(&mut enc, recording.low_latency)?;
        if let Some(intra_refresh_mode) = recording.intra_refresh_mode {
            Self::set_intra_refresh(&mut enc, intra_refresh_mode)?;
        }
        enc.enable()?;
        Ok(Encoder { enc })
    }

    pub fn input_port(&self) -> Port {
        self.enc.input_port(0)
    }

    pub fn output_port(&self) -> Port {
        self.enc.output_port(0)
    }

    pub fn start<F>(&mut self, port_cb: F) -> Result<()>
    where
        F: FnMut(&Port, &Buffer) + Send + 'static,
    {
        let pool = self.output_port().new_pool()?;

        self.enc
            .output_port(0)
            .enable_with_cb(port_cb, Some(pool))?;

        Ok(())
    }

    pub fn stop(&mut self) -> Result<()> {
        self.enc.output_port(0).disable()?;
        Ok(())
    }

    fn configure_encoder(enc: &mut obj::Component, bitrate: u32) -> Result<()> {
        let mut input = enc.input_port(0);
        let mut output = enc.output_port(0);
        input.set_compressed_input_format();
        input.commit_format()?;
        input.set_bool(ffi::MMAL_PARAMETER_VIDEO_IMMUTABLE_INPUT, true)?;
        output.set_compressed_output_format(bitrate);
        output.commit_format()?;
        Self::set_defaults(enc)?;
        Ok(())
    }

    fn set_defaults(enc: &mut obj::Component) -> Result<()> {
        Self::set_profile(enc)?;
        Ok(())
    }

    fn set_low_latency(enc: &mut obj::Component, enable: bool) -> Result<()> {
        enc.output_port(0)
            .set_bool(ffi::MMAL_PARAMETER_VIDEO_ENCODE_H264_LOW_LATENCY, enable)?;
        enc.output_port(0).set_bool(
            ffi::MMAL_PARAMETER_VIDEO_ENCODE_H264_LOW_DELAY_HRD_FLAG,
            enable,
        )?;
        Ok(())
    }

    fn set_intra_period(enc: &mut obj::Component, period: u32) -> Result<()> {
        enc.output_port(0)
            .set_uint32(ffi::MMAL_PARAMETER_INTRAPERIOD, period)?;
        Ok(())
    }

    // fn set_slices_per_frame(
    //     enc: &mut obj::Component,
    //     height: u32,
    //     slices_per_frame: u32,
    // ) -> Result<()> {
    //     let mbrows_per_slice = (height as f32 / (16_f32 * slices_per_frame as f32)).ceil() as u32;
    //
    //     println!("mbrows_per_slice = {}", mbrows_per_slice);
    //
    //     enc.output_port(0)
    //         .set_uint32(ffi::MMAL_PARAMETER_MB_ROWS_PER_SLICE, mbrows_per_slice)?;
    //
    //     Ok(())
    // }

    fn set_intra_refresh(enc: &mut obj::Component, mode: IntraRefreshMode) -> Result<()> {
        let mut param = enc
            .output_port(0)
            .get::<ffi::MMAL_PARAMETER_VIDEO_INTRA_REFRESH_T>()?;

        // let width_mb = width / 16;
        // let height_mb = height / 16;

        param.refresh_mode = mode as _;
        // param.refresh_mode = ffi::MMAL_VIDEO_INTRA_REFRESH_T_MMAL_VIDEO_INTRA_REFRESH_CYCLIC;

        // nAirMBs : Number of intra macroblocks to refresh in a frame when AIR is enabled
        // nAirRef : Number of times a motion marked macroblock has to be intra coded
        // nCirMBs : Number of consecutive macroblocks to be coded as "intra" when CIR is enabled
        // param.cir_mbs = 5000;
        // param.air_mbs = 50;
        // param.pir_mbs = 5000;
        // param.air_ref = 5000;

        enc.output_port(0)
            .set::<ffi::MMAL_PARAMETER_VIDEO_INTRA_REFRESH_T>(&mut param)?;

        Ok(())
    }

    fn set_profile(enc: &mut obj::Component) -> Result<()> {
        let mut profile = ffi::MMAL_PARAMETER_VIDEO_PROFILE_T::new();
        profile.profile[0].profile = ffi::MMAL_VIDEO_PROFILE_T_MMAL_VIDEO_PROFILE_H264_HIGH;
        // profile.profile[0].level = ffi::MMAL_VIDEO_LEVEL_T_MMAL_VIDEO_LEVEL_H264_4;
        profile.profile[0].level = ffi::MMAL_VIDEO_LEVEL_T_MMAL_VIDEO_LEVEL_H264_42;
        enc.output_port(0).set(&mut profile)?;
        Ok(())
    }
}
