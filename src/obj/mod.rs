#![allow(dead_code)]

use crate::ffi;
use crate::ffi::*;
use std::any::Any;
use std::ffi::CStr;
use std::fmt::{self, Display};
use std::ops::Deref;
use std::os::raw::c_char;
use std::panic::{self, AssertUnwindSafe};
use std::sync::Arc;
use std::sync::Condvar;
use std::sync::Mutex;
use crate::MmalError;
use std::mem::MaybeUninit;

type Result<T> = ::std::result::Result<T, MmalError>;

const VIDEO_OUTPUT_BUFFERS_NUM: u32 = 3;

bitflags! {
    pub(crate) struct BufferHeaderFlag : u32 {
        const EOS = ffi::MMAL_BUFFER_HEADER_FLAG_EOS;
        const FRAME_START = ffi::MMAL_BUFFER_HEADER_FLAG_FRAME_START;
        const FRAME_END = ffi::MMAL_BUFFER_HEADER_FLAG_FRAME_END;
        const KEYFRAME = ffi::MMAL_BUFFER_HEADER_FLAG_KEYFRAME;
        const DISCONTINUITY = ffi::MMAL_BUFFER_HEADER_FLAG_DISCONTINUITY;
        const CONFIG = ffi::MMAL_BUFFER_HEADER_FLAG_CONFIG;
        const ENCRYPTED = ffi::MMAL_BUFFER_HEADER_FLAG_ENCRYPTED;
        const CODECSIDEINFO = ffi::MMAL_BUFFER_HEADER_FLAG_CODECSIDEINFO;
        const SNAPSHOT = ffi::MMAL_BUFFER_HEADER_FLAGS_SNAPSHOT;
        const CORRUPTED = ffi::MMAL_BUFFER_HEADER_FLAG_CORRUPTED;
        const TRANSMISSION_FAILED = ffi::MMAL_BUFFER_HEADER_FLAG_TRANSMISSION_FAILED;
    }
}

impl Display for BufferHeaderFlag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.contains(BufferHeaderFlag::EOS) {
            write!(f, "EOS")?;
        }
        if self.contains(BufferHeaderFlag::FRAME_START) {
            write!(f, "FRAME_START")?;
        }
        if self.contains(BufferHeaderFlag::FRAME_END) {
            write!(f, "FRAME_END")?;
        }
        if self.contains(BufferHeaderFlag::KEYFRAME) {
            write!(f, "KEYFRAME")?;
        }
        if self.contains(BufferHeaderFlag::DISCONTINUITY) {
            write!(f, "DISCONTINUITY")?;
        }
        if self.contains(BufferHeaderFlag::CONFIG) {
            write!(f, "CONFIG")?;
        }
        if self.contains(BufferHeaderFlag::ENCRYPTED) {
            write!(f, "ENCRYPTED")?;
        }
        if self.contains(BufferHeaderFlag::CODECSIDEINFO) {
            write!(f, "CODECSIDEINFO")?;
        }
        if self.contains(BufferHeaderFlag::SNAPSHOT) {
            write!(f, "SNAPSHOT")?;
        }
        if self.contains(BufferHeaderFlag::CORRUPTED) {
            write!(f, "CORRUPTED")?;
        }
        if self.contains(BufferHeaderFlag::TRANSMISSION_FAILED) {
            write!(f, "TRANSMISSION_FAILED")?;
        }
        Ok(())
    }
}

pub(crate) trait Param {
    fn new() -> Self;
    fn header(&mut self) -> *mut crate::ffi::MMAL_PARAMETER_HEADER_T;
}

macro_rules! mmal_param {
    ($param_type:ty, $param_id:expr) => {
        impl Param for $param_type {
            fn new() -> Self {
                let hdr = crate::ffi::MMAL_PARAMETER_HEADER_T {
                    id: $param_id,
                    size: crate::mem::size_of::<$param_type>() as u32,
                };
                Self {
                    hdr,
                    ..Default::default()
                }
            }

            fn header(&mut self) -> *mut crate::ffi::MMAL_PARAMETER_HEADER_T {
                (&mut self.hdr) as *mut _
            }
        }
    };
}

mmal_param!(MMAL_PARAMETER_CAMERA_INFO_T, MMAL_PARAMETER_CAMERA_INFO);
mmal_param!(MMAL_PARAMETER_CAMERA_CONFIG_T, MMAL_PARAMETER_CAMERA_CONFIG);
mmal_param!(MMAL_PARAMETER_VIDEO_PROFILE_T, MMAL_PARAMETER_PROFILE);
mmal_param!(
    MMAL_PARAMETER_VIDEO_INTRA_REFRESH_T,
    MMAL_PARAMETER_VIDEO_INTRA_REFRESH
);
mmal_param!(
    MMAL_PARAMETER_CHANGE_EVENT_REQUEST_T,
    MMAL_PARAMETER_CHANGE_EVENT_REQUEST
);
mmal_param!(MMAL_PARAMETER_EXPOSUREMODE_T, MMAL_PARAMETER_EXPOSURE_MODE);

mmal_param!(
    MMAL_PARAMETER_DRC_T,
    MMAL_PARAMETER_DYNAMIC_RANGE_COMPRESSION
);
mmal_param!(
    MMAL_PARAMETER_CAMERA_USE_CASE_T,
    MMAL_PARAMETER_CAMERA_USE_CASE
);
mmal_param!(MMAL_PARAMETER_FLICKERAVOID_T, MMAL_PARAMETER_FLICKER_AVOID);
mmal_param!(
    MMAL_PARAMETER_EXPOSUREMETERINGMODE_T,
    MMAL_PARAMETER_EXP_METERING_MODE
);

mmal_param!(MMAL_PARAMETER_FPS_RANGE_T, MMAL_PARAMETER_FPS_RANGE);

mmal_param!(MMAL_PARAMETER_CAMERA_ANNOTATE_V4_T, MMAL_PARAMETER_ANNOTATE);

mmal_param!(MMAL_PARAMETER_AWBMODE_T, MMAL_PARAMETER_AWB_MODE);

struct PortStatus {
    panic: Mutex<Option<Box<dyn Any + Send>>>,
    cond: Condvar,
}

impl PortStatus {
    fn new() -> Self {
        Self {
            panic: Mutex::new(None),
            cond: Condvar::new(),
        }
    }

    fn panicked(&self) -> bool {
        self.panic.lock().unwrap().is_some()
    }

    fn notify_panicked(&self, panic: Box<dyn Any + Send>) {
        let mut lock = self.panic.lock().unwrap();
        *lock = Some(panic);
        self.cond.notify_one();
    }

    // fn wait_or_resume_panic(&self, seconds: u64) {
    //     let mut panic = self.panic.lock().unwrap();
    //     while panic.is_none() {
    //         let result = self
    //             .cond
    //             .wait_timeout(panic, Duration::from_secs(seconds))
    //             .unwrap();
    //         panic = result.0;
    //     }
    //     if let Some(panic) = panic.take() {
    //         panic::resume_unwind(panic);
    //     }
    // }
}

struct Userdata<F>
where
    F: FnMut(&Port, &Buffer) + Send + 'static,
{
    cb: F,
    pool: Option<Pool>,
    status: PortStatus,
}

impl<F> Userdata<F>
where
    F: FnMut(&Port, &Buffer) + Send + 'static,
{
    fn new(cb: F, pool: Option<Pool>) -> Userdata<F> {
        Userdata {
            cb,
            pool,
            status: PortStatus::new(),
        }
    }

    fn panicked(&self) -> bool {
        self.status.panicked()
    }

    fn invoke(&mut self, port: &Port, buffer: &Buffer) {
        let cb = &mut self.cb;
        let maybe_panic = panic::catch_unwind(AssertUnwindSafe(|| {
            (cb)(port, buffer);
        }));
        if let Err(e) = maybe_panic {
            self.status.notify_panicked(e);
        }
    }

    fn send_buffer(&mut self) -> Result<()> {
        if let Some(ref mut pool) = self.pool {
            pool.send_buffer()?;
        }
        Ok(())
    }

    fn send_all_buffers(&mut self) -> Result<()> {
        if let Some(ref mut pool) = self.pool {
            pool.send_all_buffers()?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub(crate) struct BufferLock {
    native: *mut ffi::MMAL_BUFFER_HEADER_T,
}

impl BufferLock {
    fn new(native: *mut ffi::MMAL_BUFFER_HEADER_T) -> Result<BufferLock> {
        mmal_call!(ffi::mmal_buffer_header_mem_lock(native))?;
        Ok(BufferLock { native })
    }
}

impl Deref for BufferLock {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe {
            let length = (*self.native).length as usize;
            let offset = (*self.native).offset as isize;
            let data = (*self.native).data;
            ::std::slice::from_raw_parts(data.offset(offset), length)
        }
    }
}

impl Drop for BufferLock {
    fn drop(&mut self) {
        unsafe {
            mmal_call_noerr!(ffi::mmal_buffer_header_mem_unlock(self.native));
        }
    }
}

pub(crate) struct Buffer {
    native: *mut ffi::MMAL_BUFFER_HEADER_T,
}

impl Buffer {
    fn wrap(native: *mut ffi::MMAL_BUFFER_HEADER_T) -> Self {
        Self { native }
    }

    pub fn lock(&self) -> Result<BufferLock> {
        BufferLock::new(self.native)
    }

    pub fn get_camera_settings(&self) -> Option<MMAL_PARAMETER_CAMERA_SETTINGS_T> {
        if self.cmd() != ffi::MMAL_EVENT_PARAMETER_CHANGED {
            return None;
        }

        #[allow(clippy::cast_ptr_alignment)]
        let param = unsafe { (*self.native).data as *mut ffi::MMAL_EVENT_PARAMETER_CHANGED_T };

        if unsafe { (*param).hdr.id } != (ffi::MMAL_PARAMETER_CAMERA_SETTINGS as u32) {
            return None;
        }

        let settings_ptr = unsafe { *(param as *mut ffi::MMAL_PARAMETER_CAMERA_SETTINGS_T) };

        Some(settings_ptr)
    }

    pub fn cmd(&self) -> u32 {
        unsafe { (*self.native).cmd }
    }

    pub fn flags(&self) -> BufferHeaderFlag {
        BufferHeaderFlag::from_bits_truncate(unsafe { (*self.native).flags })
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        mmal_call_noerr!(ffi::mmal_buffer_header_release(self.native));
    }
}

pub(crate) struct Pool {
    port: *mut MMAL_PORT_T,
    native: *mut MMAL_POOL_T,
}

impl Pool {
    pub fn send_buffer(&mut self) -> Result<()> {
        if unsafe { (*self.port).is_enabled != 0 } {
            let new_buf = mmal_call_noerr!(mmal_queue_get((*self.native).queue));
            if new_buf.is_null() {
                // XXX: this is called in a C callback, maybe panicking isn't wise here...
                // panic!("unable to get buffer from the queue");
                return Ok(());
            }
            mmal_call!(ffi::mmal_port_send_buffer(self.port, new_buf))?;
        }
        Ok(())
    }

    pub fn send_all_buffers(&mut self) -> Result<()> {
        let queue = unsafe { (*self.native).queue };
        let num = mmal_call_noerr!(ffi::mmal_queue_length(queue));
        for _ in 0..num {
            let buf = mmal_call_noerr!(ffi::mmal_queue_get(queue));
            if buf.is_null() {
                panic!("unable to get buffer from the queue");
            }
            mmal_call!(ffi::mmal_port_send_buffer(self.port, buf))?;
        }
        Ok(())
    }
}

impl Drop for Pool {
    fn drop(&mut self) {
        mmal_call_noerr!(ffi::mmal_port_pool_destroy(self.port, self.native));
    }
}

pub(crate) struct Port {
    native: *mut ffi::MMAL_PORT_T,
}

extern "C" fn native_port_callback<F>(
    port: *mut ffi::MMAL_PORT_T,
    buffer: *mut ffi::MMAL_BUFFER_HEADER_T,
) where
    F: FnMut(&Port, &Buffer) + Send + 'static,
{
    if unsafe { (*port).userdata.is_null() } {
        panic!("userdata cannot be null in native_port_callback!");
    }

    let userdata: Arc<Mutex<Userdata<F>>> = unsafe { Arc::from_raw((*port).userdata as *mut _) };

    {
        let mut ud = userdata.lock().unwrap();
        if !ud.panicked() {
            ud.invoke(&Port::wrap(port), &Buffer::wrap(buffer));
        }
        ud.send_buffer().unwrap();
    }

    // TODO: or use forget(userdata)?
    Arc::into_raw(userdata);
}

impl Port {
    fn wrap(port: *mut MMAL_PORT_T) -> Port {
        Port { native: port }
    }

    pub fn get<T: Param>(&self) -> Result<T> {
        let mut param = T::new();
        mmal_call!(ffi::mmal_port_parameter_get(
            &mut (*self.native),
            param.header()
        ))?;
        Ok(param)
    }

    pub fn set<T: Param>(&mut self, param: &mut T) -> Result<()> {
        mmal_call!(ffi::mmal_port_parameter_set(
            &mut (*self.native),
            param.header() as *const _
        ))?;
        Ok(())
    }

    pub fn connect(&mut self, other: &mut Port) -> Result<Connection> {
        unsafe {
            if (*self.native).type_ != ffi::MMAL_PORT_TYPE_T_MMAL_PORT_TYPE_OUTPUT {
                panic!("self must be an output port");
            }
            if (*other.native).type_ != ffi::MMAL_PORT_TYPE_T_MMAL_PORT_TYPE_INPUT {
                panic!("other must be an input port");
            }
        }
        let conn = Connection::new(self, other)?;
        Ok(conn)
    }

    pub fn enable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_port_enable(
            self.native,
            None as MMAL_PORT_BH_CB_T
        ))?;
        Ok(())
    }

    pub fn enable_with_cb<F>(&mut self, port_cb: F, pool: Option<Pool>) -> Result<()>
    where
        F: FnMut(&Port, &Buffer) + Send + 'static,
    {
        let cb: MMAL_PORT_BH_CB_T = Some(native_port_callback::<F>);

        let userdata = Arc::new(Mutex::new(Userdata::new(port_cb, pool)));
        unsafe {
            (*self.native).userdata = Arc::into_raw(userdata.clone()) as *mut _;
        }

        mmal_call!(ffi::mmal_port_enable(self.native, cb))?;

        userdata.lock().unwrap().send_all_buffers()?;

        Ok(())
    }

    pub fn disable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_port_disable(self.native))?;
        let userdata = unsafe { (*self.native).userdata };
        if !userdata.is_null() {
            let _: Arc<_> = unsafe { Arc::from_raw(userdata as *mut _) };
            unsafe { (*self.native).userdata = ::std::ptr::null_mut() }
        }
        Ok(())
    }

    pub fn name(&self) -> &str {
        unsafe {
            let name = (*self.native).name;
            CStr::from_ptr(name).to_str().unwrap()
        }
    }

    pub fn enabled(&self) -> bool {
        unsafe { (*self.native).is_enabled != 0 }
    }

    pub fn set_uint32(&mut self, param: u32, value: u32) -> Result<()> {
        mmal_call!(ffi::mmal_port_parameter_set_uint32(
            self.native,
            param,
            value
        ))?;
        Ok(())
    }

    pub fn set_bool(&mut self, param: u32, value: bool) -> Result<()> {
        mmal_call!(ffi::mmal_port_parameter_set_boolean(
            self.native,
            param,
            value as i32
        ))?;
        Ok(())
    }

    pub fn get_bool(&self, param: u32) -> Result<bool> {
        let mut out = 0;
        mmal_call!(ffi::mmal_port_parameter_get_boolean(
            self.native,
            param,
            &mut out
        ))?;
        Ok(out != 0)
    }

    pub fn get_uint32(&self, param: u32) -> Result<u32> {
        let mut out = 0;
        mmal_call!(ffi::mmal_port_parameter_get_uint32(
            self.native,
            param,
            &mut out
        ))?;
        Ok(out)
    }

    pub fn new_pool(&self) -> Result<Pool> {
        let pool = mmal_call_noerr!(ffi::mmal_port_pool_create(
            self.native,
            (*self.native).buffer_num,
            (*self.native).buffer_size
        ));
        if pool.is_null() {
            return Err(MmalError::with_status(
                "fail".to_string(),
                ffi::MMAL_STATUS_T::MMAL_EFAULT,
            ));
        }
        Ok(Pool {
            port: self.native,
            native: pool,
        })
    }

    pub fn commit_format(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_port_format_commit(self.native))?;
        Ok(())
    }

    pub fn copy_format(&mut self, from: &Port) -> Result<()> {
        mmal_call_noerr!(ffi::mmal_format_copy(
            (*self.native).format,
            (*from.native).format
        ));
        Ok(())
    }

    pub fn set_compressed_input_format(&mut self) {
        unsafe {
            let format = (*self.native).format;
            (*format).encoding = ffi::MMAL_ENCODING_OPAQUE;
        }
    }

    pub fn set_compressed_output_format(&mut self, bitrate: u32) {
        unsafe {
            let format = (*self.native).format;

            (*format).encoding = ffi::MMAL_ENCODING_H264;
            (*format).bitrate = bitrate;

            let es = (*format).es;
            (*es).video.frame_rate.num = 0;
            (*es).video.frame_rate.den = 1;

            (*self.native).buffer_num = (*self.native).buffer_num_recommended;
            (*self.native).buffer_size = (*self.native).buffer_size_recommended;

            if (*self.native).buffer_num < VIDEO_OUTPUT_BUFFERS_NUM {
                (*self.native).buffer_num = VIDEO_OUTPUT_BUFFERS_NUM;
            }
        }
    }

    pub fn set_opaque_format(&mut self, width: u32, height: u32, frame_rate: u32) {
        unsafe {
            let format = (*self.native).format;

            (*format).encoding = ffi::MMAL_ENCODING_OPAQUE;
            (*format).encoding_variant = ffi::MMAL_ENCODING_I420;

            let es = (*format).es;
            (*es).video.width = vcos_align_up(width, 32);
            (*es).video.height = vcos_align_up(height, 16);
            (*es).video.crop.x = 0;
            (*es).video.crop.y = 0;
            (*es).video.crop.width = width as i32;
            (*es).video.crop.height = height as i32;

            // TODO: fractional frame rates
            (*es).video.frame_rate.num = frame_rate as i32;
            (*es).video.frame_rate.den = 1;

            // TODO: hard-coded here but most likely wrong because media players
            //       actually guess what the colorspace is for raw h.264 streams
            (*es).video.color_space = ffi::MMAL_COLOR_SPACE_ITUR_BT709;

            (*self.native).buffer_num = (*self.native).buffer_num_recommended;
            (*self.native).buffer_size = (*self.native).buffer_size_recommended;
        }
    }
}

pub(crate) struct Connection {
    native: *mut ffi::MMAL_CONNECTION_T,
}

impl Connection {
    pub fn new(source: &mut Port, target: &mut Port) -> Result<Self> {
        let mut conn = MaybeUninit::<MMAL_CONNECTION_T>::uninit();
        let flags =
            ffi::MMAL_CONNECTION_FLAG_TUNNELLING | ffi::MMAL_CONNECTION_FLAG_ALLOCATION_ON_INPUT;
        mmal_call!(ffi::mmal_connection_create(
            &mut conn.as_mut_ptr(),
            source.native,
            target.native,
            flags
        ))?;
        Ok(Self { native: conn.as_mut_ptr() })
    }

    pub fn enabled(&self) -> bool {
        unsafe { (*self.native).is_enabled != 0 }
    }

    pub fn enable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_connection_enable(self.native))?;
        Ok(())
    }

    pub fn disable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_connection_disable(self.native))?;
        Ok(())
    }

    pub fn name(&self) -> &str {
        unsafe {
            let name = (*self.native).name;
            CStr::from_ptr(name).to_str().unwrap()
        }
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        mmal_call!(ffi::mmal_connection_destroy(self.native)).unwrap();
    }
}

pub(crate) struct Component {
    native: *mut ffi::MMAL_COMPONENT_T,
}

impl Component {
    pub fn new(comp_type: &[u8]) -> Result<Self> {
        unsafe {
            let comp_type = comp_type.as_ptr() as *const c_char;
            let mut component = MaybeUninit::<ffi::MMAL_COMPONENT_T>::uninit();
            mmal_call!(ffi::mmal_component_create(comp_type, &mut component.as_mut_ptr()))?;
            Ok(Self { native: component.as_mut_ptr() })
        }
    }

    pub fn enabled(&self) -> bool {
        unsafe { (*self.native).is_enabled != 0 }
    }

    pub fn enable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_component_enable(self.native))?;
        Ok(())
    }

    pub fn disable(&mut self) -> Result<()> {
        mmal_call!(ffi::mmal_component_disable(self.native))?;
        Ok(())
    }

    pub fn num_output_ports(&self) -> isize {
        unsafe { (*self.native).output_num as isize }
    }

    pub fn num_input_ports(&self) -> isize {
        unsafe { (*self.native).input_num as isize }
    }

    pub fn output_port(&self, index: isize) -> Port {
        let output_num = self.num_output_ports();
        if index >= output_num {
            panic!("index out of range: {}, maximum: {}", index, output_num);
        }
        let outputs: *mut *mut MMAL_PORT_T = unsafe { (*self.native).output };
        let port: *mut MMAL_PORT_T = unsafe { *outputs.offset(index) };
        Port::wrap(port)
    }

    pub fn input_port(&self, index: isize) -> Port {
        let input_num = self.num_input_ports();
        if index >= input_num {
            panic!("index out of range: {}, maximum: {}", index, input_num);
        }
        let inputs: *mut *mut MMAL_PORT_T = unsafe { (*self.native).input };
        let port: *mut MMAL_PORT_T = unsafe { *inputs.offset(index) };
        Port::wrap(port)
    }

    pub fn control_port(&self) -> Port {
        Port::wrap(unsafe { (*self.native).control })
    }
}

impl Drop for Component {
    fn drop(&mut self) {
        for i in 0..self.num_input_ports() {
            let mut port = self.input_port(i);
            if port.enabled() {
                port.disable().unwrap();
            }
        }
        for i in 0..self.num_output_ports() {
            let mut port = self.output_port(i);
            if port.enabled() {
                port.disable().unwrap();
            }
        }
        mmal_call!(ffi::mmal_component_destroy(self.native)).unwrap();
    }
}
