//! Access the native Raspberry Pi camera.
//!
//! This module uses the MMAL library ([mmal-sys]) to access the Raspberry Pi's camera
//! in a friendly way.
//!
//! [mmal-sys]: https://crates.io/crates/mmal-sys

#[macro_use]
extern crate bitflags;
extern crate mmal_sys as ffi;

use std::fmt;
use std::mem;
use std::sync::Mutex;
use std::sync::{Arc, Once};
use std::time::Duration;

#[macro_use]
mod error;
mod encoder;
use crate::encoder::Encoder;
pub use crate::encoder::{Recording, RecordingBuilder};
mod enums;

#[macro_use]
mod obj;
use crate::obj::*;

#[macro_use]
mod props;

pub use crate::enums::*;
pub use crate::error::{CameraError, MmalError};

type Result<T> = ::std::result::Result<T, crate::error::CameraError>;

fn init() {
    static INIT: Once = Once::new();
    INIT.call_once(|| unsafe {
        ffi::bcm_host_init();
    });
}

#[derive(Clone, Copy, Eq, PartialEq)]
struct Config {
    pub sensor_mode: u32,
    pub width: u32,
    pub height: u32,
    pub frame_rate: u32,
}

impl fmt::Debug for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.sensor_mode != 0 {
            write!(
                f,
                "{}x{}@{} (mode: {})",
                self.width, self.height, self.frame_rate, self.sensor_mode
            )
        } else {
            write!(f, "{}x{}@{}", self.width, self.height, self.frame_rate)
        }
    }
}

impl Default for Config {
    fn default() -> Config {
        Config {
            sensor_mode: 0,
            width: 1280,
            height: 720,
            frame_rate: 30,
        }
    }
}

bitflags! {
    /// Frame flags.
    #[derive(Default)]
    pub struct FrameFlag : u32 {
        /// Frame is a key frame.
        const KEYFRAME = 1;

        /// Frame contains configuration NALs (SPS / PPS).
        const CONFIG = 2;
    }
}

/// Metadata about a captured frame.
#[derive(Debug, Default, Clone, Copy)]
pub struct FrameInfo {
    shutter_speed: u32,
    analog_gain: f32,
    digital_gain: f32,
}

impl FrameInfo {
    /// Get the frame's shutter speed.
    pub fn shutter_speed(&self) -> Duration {
        Duration::from_micros(self.shutter_speed.into())
    }

    /// Get the frame's analog gain.
    pub fn analog_gain(&self) -> f32 {
        self.analog_gain
    }

    /// Get the frame's digital gain.
    pub fn digital_gain(&self) -> f32 {
        self.digital_gain
    }
}

/// The Raspberry Pi camera.
pub struct Camera {
    cam: Component,
    config: Config,
    frame_info: Arc<Mutex<FrameInfo>>,
    error: Arc<Mutex<Option<CameraError>>>,
    recording: Option<(Encoder, Connection)>,
}

impl Camera {
    const MMAL_CAMERA_PREVIEW_PORT: isize = 0;
    const MMAL_CAMERA_VIDEO_PORT: isize = 1;
    const MMAL_CAMERA_CAPTURE_PORT: isize = 2;

    /// Initialize the Raspberry Pi camera and enable it for use.
    /// May fail if the camera is not connected.
    pub fn new() -> Result<Camera> {
        init();
        let mut cam = Camera {
            cam: Component::new(ffi::MMAL_COMPONENT_DEFAULT_CAMERA)?,
            config: Default::default(),
            frame_info: Arc::new(Mutex::new(FrameInfo::default())),
            error: Arc::new(Mutex::new(None)),
            recording: None,
        };
        let cfg = cam.config;
        cam.configure_camera(cfg)?;
        cam.cam.enable()?;
        Ok(cam)
    }

    /// Begin recording with the given configuration and callback.
    pub fn start_recording<F>(&mut self, recording: Recording, callback: F) -> Result<()>
    where
        F: FnMut(&FrameInfo, FrameFlag, &[u8]) + Send + 'static,
    {
        self.check_cb_error()?;
        if self.recording.is_some() {
            self.stop_recording()?;
        }
        let mut enc = Encoder::new(self.config.width, self.config.height, recording)?;
        let mut conn = self.video_port().connect(&mut enc.input_port())?;
        conn.enable()?;
        let frame_info_mutex = self.frame_info.clone();
        let mut cb = callback;
        enc.start(move |_, buffer| {
            let frame_info = *frame_info_mutex.lock().unwrap();
            cb(&frame_info, FrameFlag::empty(), &*buffer.lock().unwrap());
        })?;
        self.video_port()
            .set_bool(ffi::MMAL_PARAMETER_CAPTURE, true)?;
        self.recording = Some((enc, conn));
        Ok(())
    }

    /// Stop a recording in progress.
    pub fn stop_recording(&mut self) -> Result<()> {
        self.check_cb_error()?;
        if let Some((mut enc, mut conn)) = self.recording.take() {
            self.video_port()
                .set_bool(ffi::MMAL_PARAMETER_CAPTURE, false)?;
            conn.disable()?;
            enc.stop()?;
        }
        Ok(())
    }

    /// Check if a recording is currently in progress.
    pub fn is_recording(&self) -> bool {
        self.recording.is_some()
    }

    fn check_cb_error(&self) -> Result<()> {
        let maybe_error: Option<CameraError> = self.error.lock().unwrap().take();
        if let Some(error) = maybe_error {
            return Err(error);
        }
        Ok(())
    }

    fn set_config_property(&mut self, mut f: impl FnMut(&mut Self) -> Result<()>) -> Result<()> {
        self.check_cb_error()?;
        if self.recording.is_some() {
            panic!("cannot change this property while recording");
        }
        self.cam.disable()?;
        f(self)?;
        self.cam.enable()?;
        Ok(())
    }

    /// Set capture resolution.
    pub fn set_resolution(&mut self, width: u32, height: u32) -> Result<()> {
        self.set_config_property(|cam| {
            let mut cfg = cam.config;
            cfg.width = width;
            cfg.height = height;
            cam.configure_camera(cfg)
        })
    }

    /// Get capture resolution.
    pub fn resolution(&self) -> Result<(u32, u32)> {
        Ok((self.config.width, self.config.height))
    }

    /// Set recording framerate.
    pub fn set_framerate(&mut self, framerate: u32) -> Result<()> {
        self.set_config_property(|cam| {
            let mut cfg = cam.config;
            cfg.frame_rate = framerate;
            cam.configure_camera(cfg)
        })
    }

    /// Get recording framerate.
    pub fn framerate(&self) -> Result<u32> {
        Ok(self.config.frame_rate)
    }

    /// Set the sensor mode.
    pub fn set_sensor_mode(&mut self, sensor_mode: u32) -> Result<()> {
        self.set_config_property(|cam| {
            let mut cfg = cam.config;
            cfg.sensor_mode = sensor_mode;
            cam.configure_camera(cfg)
        })
    }

    /// Get the sensor mode.
    pub fn sensor_mode(&self) -> Result<u32> {
        Ok(self.config.sensor_mode)
    }

    /// Exposure mode.
    property_enum!(
        exposure_mode,
        set_exposure_mode,
        ExposureMode,
        value,
        ffi::MMAL_PARAMETER_EXPOSUREMODE_T
    );

    /// Metering mode.
    property_enum!(
        metering_mode,
        set_metering_mode,
        MeteringMode,
        value,
        ffi::MMAL_PARAMETER_EXPOSUREMETERINGMODE_T
    );

    /// White-balance mode.
    property_enum!(
        awb_mode,
        set_awb_mode,
        AwbMode,
        value,
        ffi::MMAL_PARAMETER_AWBMODE_T
    );

    /// Dynamic range compression (DRC).
    property_enum!(
        dynamic_range_compression,
        set_dynamic_range_compression,
        DrcStrength,
        strength,
        ffi::MMAL_PARAMETER_DRC_T
    );

    /// ISO sensitivity.
    property_uint32!(iso, set_iso, ffi::MMAL_PARAMETER_ISO);

    /// Exposure compenstation.
    property_uint32!(
        exposure_compensation,
        set_exposure_compensation,
        ffi::MMAL_PARAMETER_EXPOSURE_COMP
    );

    /// Shutter speed.
    property_uint32!(
        shutter_speed,
        set_shutter_speed,
        ffi::MMAL_PARAMETER_SHUTTER_SPEED
    );

    /// Video stabilization.
    property_bool!(
        video_stabilisation,
        set_video_stabilisation,
        ffi::MMAL_PARAMETER_VIDEO_STABILISATION
    );

    /// Video denoise.
    property_bool!(
        video_denoise,
        set_video_denoise,
        ffi::MMAL_PARAMETER_VIDEO_DENOISE
    );

    fn control_port_callback(
        error_mutex: &Arc<Mutex<Option<CameraError>>>,
        frame_info_mutex: &Arc<Mutex<FrameInfo>>,
        buffer: &Buffer,
    ) {
        match buffer.cmd() {
            ffi::MMAL_EVENT_ERROR => {
                *error_mutex.lock().unwrap() = Some(error::no_data_received());
            }
            ffi::MMAL_EVENT_PARAMETER_CHANGED => {
                if let Some(settings) = buffer.get_camera_settings() {
                    *frame_info_mutex.lock().unwrap() = FrameInfo {
                        shutter_speed: settings.exposure,
                        analog_gain: settings.analog_gain.num as f32
                            / settings.analog_gain.den as f32,
                        digital_gain: settings.digital_gain.num as f32
                            / settings.digital_gain.den as f32,
                    };
                } else {
                    eprintln!("couldn't get camera settings ...");
                }
            }
            _ => {
                *error_mutex.lock().unwrap() = Some(error::unknown_camera_event());
            }
        }
    }

    fn configure_camera(&mut self, desired_cfg: Config) -> Result<()> {
        if self.cam.enabled() {
            panic!("camera must be disabled to be configured");
        }

        self.control_port().set_uint32(
            ffi::MMAL_PARAMETER_CAMERA_CUSTOM_SENSOR_CONFIG,
            desired_cfg.sensor_mode,
        )?;

        if !self.control_port().enabled() {
            let mut change_req = ffi::MMAL_PARAMETER_CHANGE_EVENT_REQUEST_T::new();
            change_req.change_id = ffi::MMAL_PARAMETER_CAMERA_SETTINGS;
            change_req.enable = true as i32;
            self.cam.control_port().set(&mut change_req)?;

            let err_mutex = self.error.clone();
            let frame_info_mutex = self.frame_info.clone();

            self.control_port().enable_with_cb(
                move |_, buf| {
                    let err_mutex = &err_mutex;
                    let frame_info_mutex = &frame_info_mutex;
                    Self::control_port_callback(err_mutex, frame_info_mutex, buf);
                },
                None,
            )?;
        }

        let mut cur_cfg = self
            .control_port()
            .get::<ffi::MMAL_PARAMETER_CAMERA_CONFIG_T>()?;

        cur_cfg.stills_yuv422 = 0;
        cur_cfg.one_shot_stills = 1;
        cur_cfg.max_stills_w = desired_cfg.width;
        cur_cfg.max_stills_h = desired_cfg.height;
        cur_cfg.max_preview_video_w = desired_cfg.width;
        cur_cfg.max_preview_video_h = desired_cfg.height;
        cur_cfg.num_preview_video_frames = 3; // + vcos_max(0, (frame_rate - 30) / 10),
        cur_cfg.fast_preview_resume = 0;
        cur_cfg.stills_capture_circular_buffer_height = 0;
        cur_cfg.use_stc_timestamp =
            ffi::MMAL_PARAMETER_CAMERA_CONFIG_TIMESTAMP_MODE_T_MMAL_PARAM_TIMESTAMP_MODE_RAW_STC;

        self.control_port().set(&mut cur_cfg)?;

        self.video_port().set_opaque_format(
            desired_cfg.width,
            desired_cfg.height,
            desired_cfg.frame_rate,
        );
        self.video_port().commit_format()?;
        self.preview_port()
            .set_opaque_format(desired_cfg.width, desired_cfg.height, 0);
        self.preview_port().commit_format()?;
        self.still_port()
            .set_opaque_format(desired_cfg.width, desired_cfg.height, 0);
        self.still_port().commit_format()?;

        Ok(())
    }

    fn still_port(&self) -> Port {
        self.cam.output_port(Self::MMAL_CAMERA_CAPTURE_PORT)
    }

    fn video_port(&self) -> Port {
        self.cam.output_port(Self::MMAL_CAMERA_VIDEO_PORT)
    }

    fn preview_port(&self) -> Port {
        self.cam.output_port(Self::MMAL_CAMERA_PREVIEW_PORT)
    }

    fn control_port(&self) -> Port {
        self.cam.control_port()
    }
}

impl Drop for Camera {
    fn drop(&mut self) {
        self.check_cb_error().unwrap();
        if self.is_recording() {
            self.stop_recording().unwrap();
        }
    }
}
