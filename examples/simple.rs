extern crate rascam;

use rascam::{Camera, CameraError, Recording};
use std::thread;
use std::time::Duration;

fn main() -> Result<(), CameraError> {
    let mut cam = Camera::new()?;
    cam.set_resolution(640, 480)?;
    cam.set_framerate(25)?;

    cam.start_recording(Recording::default(), |meta, flags, buf| {
        println!("{:?}, {:?}, {}", meta, flags, buf.len());
    })?;

    thread::sleep(Duration::from_secs(5));

    Ok(())
}
